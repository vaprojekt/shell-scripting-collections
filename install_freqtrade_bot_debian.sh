#!/bin/sh 
dist=$(grep -Po "(?<=^ID=).+" /etc/os-release | sed 's/"//g')

LIST_OF_APPS=""
if [[ "$dist" == "debian" ]]; then
    LIST_OF_APPS="gdm3 nano autocutsel xorg lxde-core firefox-esr ufw cifs-utils rsync remmina tightvncserver software-properties-common expect curl htop  tmux wget python3-pip python3-venv python3-dev python3-pandas git build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev libsqlite3-dev wget libbz2-dev python3-dev"
else
    LIST_OF_APPS="gdm3 nano autocutsel xorg lxde-core firefox ufw cifs-utils rsync remmina tightvncserver software-properties-common expect curl htop  tmux wget python3-pip python3-venv python3-dev python3-pandas git build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev libsqlite3-dev wget libbz2-dev python3-dev"
fi

sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get install -y $LIST_OF_APPS
sudo ufw enable
sudo ufw allow ssh


git clone https://github.com/freqtrade/freqtrade.git
cd /home/debian/freqtrade
git checkout develop


#Set password for vnc
myuser=$(whoami)
mypasswd="put your strong password here"
my_path=$HOME


mkdir $my_path/.vnc
/usr/bin/expect << EOF
spawn /usr/bin/vncpasswd $my_path/.vnc/passwd
#spawn vncserver :1 -geometry 1280x960 -depth 16 -pixelformat rgb565 -localhost
expect "Password:"
send "$mypasswd\r"
expect "Verify:"
send "$mypasswd\r"
expect "Would"
send "n\r"
expect eof
exit
EOF

##############
#Config vnc

cat << EOT >> $my_path/.vnc/xstartup
#!/bin/sh

xrdb $HOME/.Xresources
# -solid grey gaves us a real mouse pointer instead of the default X
xsetroot -solid grey -cursor_name left_ptr
# Allow copy & paste when ClientCutText is set to true on the client side
autocutsel -fork
#x-terminal-emulator -geometry 80x24+10+10 -ls -title "$VNCDESKTOP Desktop" &
#x-window-manager &
# Fix to make GNOME work
export XKL_XMODMAP_DISABLE=1
/etc/X11/Xsession
lxterminal &
/usr/bin/lxsession -s LXDE &
EOT

##############
chmod +x $my_path/.vnc/xstartup
sudo /usr/bin/vncserver :1 -geometry 1280x960 -depth 16 -pixelformat rgb565 -localhost
####################################################

/usr/bin/expect << EOF
set timeout -1
spawn /home/debian/freqtrade/setup.sh -i
expect "Reset"
send "y\r"
expect "dependencies for dev"
send "y\r"
expect "plotting"
send "y\r"
expect "hyperopt"
send "y\r"
expect "dependencies for freqai"
send "y\r"
expect eof
exit
EOF

/home/debian/freqtrade/.env/bin/freqtrade install-ui --ui-version 0.3.5
cat << EOT >> /home/debian/requirements.txt
py3cw
pandas_ta
PyWavelets
simdkalman
pykalman
EOT
#source /home/debian/freqtrade/.env/bin/activate
/home/debian/freqtrade/.env/bin/pip install -r /home/debian/requirements.txt

mkdir /home/debian/freqtrade/config

chown -R debian:debian /home/debian/freqtrade
sudo add-apt-repository ppa:longsleep/golang-backports -y && sudo apt update && sudo apt install -y golang-go && sudo apt install -y golang-easyjson

mkdir /home/debian/local_share
/usr/bin/expect << EOF
spawn mount -t cifs -o username=freqtradebackup //samba_server_ip/share/freqtrade /home/debian/local_share
expect "Password"
send "samba_password\r"
expect eof
exit
EOF

###############
cat << EOT >> /home/debian/backup-data.sh
#!/bin/sh
rsync -Pau "/home/debian/freqtrade/config/" "/home/debian/local_share/config"
rsync -Pau "/home/debian/freqtrade/user_data/" "/home/debian/local_share/user_data"
EOT

chmod +x /home/debian/backup-data.sh

(crontab -u root -l 2>/dev/null || true; echo "@hourly /home/debian/backup-data.sh >>/home/debian/cron_backup-data_debug.log 2>&1") | crontab -u root -

dist=$(grep -Po "(?<=^ID=).+" /etc/os-release | sed 's/"//g')

if [[ "$dist" == "debian" ]]; then
    sudo apt install wget software-properties-common apt-transport-https -y
    wget https://golang.org/dl/go1.17.linux-amd64.tar.gz
    sudo tar -zxvf go1.17.linux-amd64.tar.gz -C /usr/local/
    echo "export PATH=/usr/local/go/bin:${PATH}" | sudo tee /etc/profile.d/go.sh
    source /etc/profile.d/go.sh
    mkdir ~/.go
    echo "GOPATH=$HOME/.go" >> ~/.bashrc
    echo "export GOPATH" >> ~/.bashrc
    echo "PATH=\$PATH:\$GOPATH/bin # Add GOPATH/bin to PATH for scripting" >> ~/.bashrc
    source ~/.bashrc
    go get github.com/mailru/easyjson/ && go install github.com/mailru/easyjson/...@latest
fi

cd /home/debian/freqtrade/
git clone https://github.com/mikekonan/exchange-proxy.git
cd /home/debian/freqtrade/exchange-proxy
sudo make build #if error file not found in path so run this command: make build 

sudo tee -a /etc/systemd/system/exchange-proxy.service > /dev/null <<EOT
[Unit]
Description=exchange-proxy service
After=network.target
StartLimitIntervalSec=0
[Service]
Type=simple
Restart=always
#Restart=on-failure
RestartSec=1
#User=debian
User=root
#ExecStart=/usr/bin/env php /path/to/server.php
#ExecStart=/usr/bin/env ./exchange-proxy/dist/exchange-proxy -port 8070 -verbose 1 &
ExecStart=/usr/bin/env /home/debian/freqtrade/exchange-proxy/dist/exchange-proxy -port 8070 -verbose 1 &
#ExecStart=/usr/bin/env /home/debian/freqtrade/exchange-proxy/dist/exchange-proxy -port 8070 -verbose 1 &
#ExecStart=/usr/bin/env /home/debian/exchange-proxy/dist/exchange-proxy -port 8070 -verbose 1 &

[Install]
WantedBy=multi-user.target
EOT


cat << EOT >> /home/debian/restart_exchange_proxy.sh
#!/bin/sh
systemctl stop exchange-proxy.service
systemctl start exchange-proxy.service
systemctl daemon-reload
EOT

chmod +x /home/debian/restart_exchange_proxy.sh
(crontab -u root -l 2>/dev/null || true; echo "@hourly /home/debian/restart_exchange_proxy.sh >>/home/debian/cron_restart_exchange_proxy_debug.log 2>&1") | crontab -u root -

#(crontab -u root -l || true; echo "@hourly /home/debian/restart_exchange_proxy.sh >>/home/debian/cron_restart_exchange_proxy_debug.log 2>&1") | crontab -u root -

/home/debian/restart_exchange_proxy.sh
sudo systemctl start exchange-proxy.service
sudo systemctl enable exchange-proxy.service
#sudo systemctl status exchange-proxy.service

#(crontab -u $(whoami) -l ...)
#############
wget https://github.com/nightshift2k/binance-proxy/releases/download/v1.2.4/binance-proxy_1.2.4_Linux_x86_64.tar.gz
tar -xf binance-proxy_1.2.4_Linux_x86_64.tar.gz -C /usr/local/bin 

sudo tee -a /etc/systemd/system/binance-proxy.service > /dev/null << EOT
[Unit]
Description=binance-proxy service
After=network.target
StartLimitIntervalSec=0
[Service]
Type=simple
Restart=always
#Restart=on-failure
RestartSec=1
#User=debian
User=root
#ExecStart=/usr/bin/env php /path/to/server.php
#ExecStart=/usr/bin/env ./exchange-proxy/dist/exchange-proxy -port 8070 -verbose 1 &
ExecStart=/usr/local/bin/binance-proxy -p 8060 -t 8061

[Install]
WantedBy=multi-user.target
EOT


cat << EOT >> /home/debian/restart_binance-proxy.sh
#!/bin/sh
systemctl stop binance-proxy.service
systemctl start binance-proxy.service
systemctl daemon-reload
EOT

chmod +x /home/debian/restart_binance-proxy.sh
(crontab -u root -l 2>/dev/null || true; echo "@hourly /home/debian/restart_binance-proxy.sh >>/home/debian/cron_restart_binance-proxy_debug.log 2>&1") | crontab -u root -
/home/debian/restart_binance-proxy.sh

sudo systemctl start binance-proxy.service
sudo systemctl enable binance-proxy.service
#sudo systemctl status binance-proxy.service

################
git clone https://github.com/tmux-plugins/tpm /home/debian/.tmux/plugins/tpm
cp /usr/share/doc/tmux/example_tmux.conf /home/debian/.tmux.conf

cat << EOT >> /home/debian/.tmux.conf
# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'
set -g @continuum-restore 'on'
set -g @continuum-boot 'on'

# Other examples:
# set -g @plugin 'github_username/plugin_name'
# set -g @plugin 'github_username/plugin_name#branch'
# set -g @plugin 'git@github.com:user/plugin'
# set -g @plugin 'git@bitbucket.com:user/plugin'

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '/home/debian/.tmux/plugins/tpm/tpm'
EOT

tmux source /home/debian/.tmux.conf