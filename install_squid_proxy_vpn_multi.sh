#!/bin/sh 
VM_PASS='put your passwd'

createVMBash() {
#Create install proxy bash file for every vm on the main vm(vm connect to az cli)
cat << EOT >> /home/ubuntu/install_squid_proxy.sh
#!/bin/sh 
sudo apt-get install curl wget iptables
wget https://raw.githubusercontent.com/serverok/squid-proxy-installer/master/squid3-install.sh -O /home/ubuntu/squid3-install.sh
sudo bash /home/ubuntu/squid3-install.sh
sudo /usr/bin/htpasswd -b -c /etc/squid/passwd $USERNAME $PASSWORD
sed -i "s/^http_port.*$/http_port $PORT/g"  /etc/squid/squid.conf	
/sbin/iptables -I INPUT -p tcp --dport $PORT -j ACCEPT
/sbin/iptables-save
sudo systemctl start squid
sudo systemctl enable squid
sudo systemctl restart squid
sudo systemctl reload squid	
EOT

#Create install vpn bash file for every vm
cat << EOT >> /home/ubuntu/install_vpn.sh
#!/bin/sh 
wget https://get.vpnsetup.net -O /home/ubuntu/vpn.sh
sed -i "s/YOUR_IPSEC_PSK=''/YOUR_IPSEC_PSK='$PSK'/g" /home/ubuntu/vpn.sh
sed -i "s/YOUR_USERNAME=''/YOUR_USERNAME='$USERNAME'/g" /home/ubuntu/vpn.sh
sed -i "s/YOUR_PASSWORD=''/YOUR_PASSWORD='$PASSWORD'/g" /home/ubuntu/vpn.sh	
sudo bash /home/ubuntu/vpn.sh
EOT

#Set execute permission for created bash file
chmod +x /home/ubuntu/install_squid_proxy.sh
chmod +x /home/ubuntu/install_vpn.sh
}

createInfo() {
USERNAME=$(tr -dc a-z </dev/urandom|head -c 3)$(tr -dc a-z0-9 </dev/urandom | head -c 7)
#PASSWORD=$(tr -dc A-Za-z </dev/urandom|head -c 1)$(LC_ALL=C tr -dc 'A-Za-z0-9!"#$%&'\''()*+,-./;<=>?@[\]^_`{|}~' </dev/urandom | head -c 11)
PASSWORD=$(tr -dc A-Za-z </dev/urandom|head -c 1)$(LC_ALL=C tr -dc A-Za-z0-9 </dev/urandom | head -c 5)$(tr -dc '!#%^)+,-.=?@]_~' </dev/urandom | head -c 4)$(tr -dc A-Za-z </dev/urandom|head -c 2)
PORT=$(tr -dc 3-9 </dev/urandom | head -c 1)$(tr -dc 0-9 </dev/urandom | head -c 3)
#PSK=$(head -c 24 /dev/urandom | base64)
PSK=$(tr -dc A-Za-z </dev/urandom|head -c 1)$(LC_ALL=C tr -dc 'A-Za-z0-9!#%^)+,-.=?@]_~' </dev/urandom | head -c 21)$(tr -dc A-Za-z </dev/urandom|head -c 2)
}

saveInfo() {
	#Open port for proxy, vpn
	#az vm open-port --resource-group $RGroup --name $CName --port 80,443,22,$PORT,500,4500
	#Save proxy info to file
	echo $IP":"$PORT":"$USERNAME":"$PASSWORD":"$PSK >> proxy.txt		
}
clearInfo() {
	#Then rm /home/ubuntu/install_squid_proxy.sh
	sudo /bin/rm /home/ubuntu/install_squid_proxy.sh
	sudo /bin/rm /home/ubuntu/install_vpn.sh
}

sendFileToRemoteNRun() {
	sudo ssh-keygen -f "/root/.ssh/known_hosts" -R $IP
	#Send bash files to remote server over ssh and run files as root
	sshpass -p $VM_PASS ssh -oStrictHostKeyChecking=no ubuntu@$IP 'sudo bash -s' < /home/ubuntu/install_squid_proxy.sh
	sshpass -p $VM_PASS ssh -oStrictHostKeyChecking=no ubuntu@$IP 'sudo bash -s' < /home/ubuntu/install_vpn.sh
	#RM files on remote server
	sshpass -p $VM_PASS ssh -oStrictHostKeyChecking=no ubuntu@$IP 'sudo /bin/rm /home/ubuntu/*.sh'
}

#Find the name and resource group of vps public_ip = $IP publicIp get from output of az vm create
IPS=$(az vm list -d --query "[?powerState=='VM running'].{IP:publicIps}" -o tsv)
for IP in $IPS
do
CName=$(az vm list -d --query "[?powerState=='VM running' && publicIps=='$IP'].{VM:name}" -o tsv)
RGroup=$(az vm list -d --query "[?powerState=='VM running' && publicIps=='$IP'].{RG:resourceGroup}" -o tsv)
#ClearInfo
clearInfo
#Create Info
createInfo
#createVMBash
createVMBash
#sendFileToRemoteNRun
sendFileToRemoteNRun
#Open port for proxy, vpn
az vm open-port --resource-group $RGroup --name $CName --port 80,443,22,$PORT,500,4500,1701
#Save proxy info to file
saveInfo
done