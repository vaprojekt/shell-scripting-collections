#!/bin/bash
ARCH=$(dpkg --print-architecture)
ACCESS_KEY=""
SECRET_KEY=""
DIST=$(grep -Po "(?<=^ID=).+" /etc/os-release | sed 's/"//g')
HOME_DIR='/home/ubuntu'

if [ "$DIST" = "debian" ]; then
    HOME_DIR='/home/'$DIST
fi

WORK_DIR=$HOME_DIR/terraform_aws
KEY_DIR=$HOME_DIR/.key
RANDOM_NR_OF_VM=$((RANDOM%4+2)) #$((RANDOM%5+2)) random_num from 2-5
SCRIPT_DIR=$WORK_DIR/scripts

initFolder() {
if [ ! -d "$HOME_DIR" ]; then
   mkdir $HOME_DIR
   mkdir $WORK_DIR
   mkdir $SCRIPT_DIR
fi

if [ ! -d "$WORK_DIR" ]; then
   mkdir $WORK_DIR
   mkdir $SCRIPT_DIR
fi

if [ -d "$SCRIPT_DIR/.key" ]; then
   mv $SCRIPT_DIR/.key $HOME_DIR/
fi

mv $HOME_DIR/*_aws.txt $WORK_DIR/personal_access_token.txt
#https://stackoverflow.com/questions/2613800/how-to-convert-dos-windows-newline-crlf-to-unix-newline-lf
sed -i $'s/\r$//' $WORK_DIR/personal_access_token.txt
mv $HOME_DIR/install_earn_with_traffic_* $SCRIPT_DIR/install_earn_with_traffic_wo_sudo
cp $HOME_DIR/terraform_ $SCRIPT_DIR/terraform_aws
}

createRandomRegion() {
#This type of array work only with #!/bin/bash , /bin/sh dont work
a=(us-east-2 us-east-1 us-west-1 us-west-2 ap-east-1 ap-south-1 ap-northeast-3 ap-northeast-2 ap-northeast-1 ap-southeast-1 ap-southeast-2 ca-central-1 eu-central-1 eu-west-1 eu-west-2 eu-west-3 eu-north-1 sa-east-1)
#for ((i=0; i<${#a[@]}; i+=1)); do echo "${a[i]}"; done  
size=${#a[@]}
index=$(($RANDOM % $size))
AWS_REGION=${a[$index]}
}

#https://stackoverflow.com/questions/67210801/terraform-aws-instance-changing-volume-size
createTerrafromCfg() {
cat << EOT > $WORK_DIR/aws-terraform.tf
#Aws Provider definition
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }
}

provider "aws" {
  region     = "$AWS_REGION"
  access_key = var.access_key
  secret_key = var.secret_key
}

resource "aws_key_pair" "key_pair" {
  #key_name   = "test-key"
  public_key = "\${file(var.ssh_public_key)}"
}

#http://cavaliercoder.com/blog/inline-vs-discrete-security-groups-in-terraform.html
resource "aws_security_group" "allow_all" {
  #name        = "allow_all"
  description = "Allow all inbound traffic"
}

resource "aws_security_group_rule" "ingress" {
  type        = "ingress"
  from_port   = 0
  to_port     = 0 #65535
  protocol    = "-1"	#"tcp"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "\${aws_security_group.allow_all.id}"
}

resource "aws_security_group_rule" "egress" {
  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = -1
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "\${aws_security_group.allow_all.id}"
}

resource "random_pet" "project" {
  prefix    = "tf_test"
  separator = "_"
}

data "aws_ami" "windows_ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    #values = ["Windows_Server-2012-R2_RTM-English-64Bit-Base-*"]
    values = ["Windows_Server-2019-English-Full-ContainersLatest*"]
  }
}

data "aws_ami" "ubuntu" {

    most_recent = true

    filter {
        name   = "name"
        values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
    }

    filter {
        name = "virtualization-type"
        values = ["hvm"]
    }

    owners = ["099720109477"]
}

resource "random_shuffle" "instance_type" {
  input        = ["t2.micro", "t2.small", "t3.micro"]
  result_count = 2
}

resource "aws_instance" "ec2" {
  count = var.instance_count
  ami               = "\${data.aws_ami.ubuntu.image_id}"
  instance_type     = "\${random_shuffle.instance_type.result[count.index%2]}"
  key_name          = "\${aws_key_pair.key_pair.key_name}"
  security_groups   = ["\${aws_security_group.allow_all.name}"]
  #user_data     = file("$SCRIPT_DIR/install_earn_with_traffic_wo_sudo.sh")

  # root disk
  # encrypted             = true
  root_block_device {
    volume_size           = "32"
    volume_type           = "gp2"
    delete_on_termination = true
  }

  tags = {
    Name = "\${random_pet.project.id}_\${count.index + 1}"
  }  
}

resource "null_resource" "run_script" {
	depends_on = [aws_instance.ec2]
	count = var.instance_count
	
	provisioner "file" {
		source      = "$SCRIPT_DIR/install_earn_with_traffic_wo_sudo" #install_earn_with_traffic_wo_sudo.sh.x
		destination = "/tmp/install_earn_with_traffic_wo_sudo"	#install_earn_with_traffic_wo_sudo.sh.x
	}

	provisioner "file" {
	  	source      = "$SCRIPT_DIR/"
	 	destination = "/tmp"
	}

	# Change permissions on bash script and execute from ec2-user.
	provisioner "remote-exec" {
		inline = [
			"sudo chmod +x /tmp/install_*",
			"sudo bash /tmp/install_cronjob.sh",	
		]
		
		# Login to the ec2-user with the aws key.
		connection {
			type        = "ssh"
			user        = "ubuntu"
			private_key = file(var.ssh_private_key)
			host        = "\${aws_instance.ec2[count.index].public_ip}"
			timeout     = "4m"
		}
	}
	  
	# Login to the ec2-user with the aws key.
	connection {
		type        = "ssh"
		user        = "ubuntu"
		private_key = file(var.ssh_private_key)
		host        = "\${aws_instance.ec2[count.index].public_ip}"
		timeout     = "4m"
	}
}

resource "null_resource" "get_proxies" {
	depends_on = [aws_instance.ec2]
	count = var.instance_count
	provisioner "local-exec" {
		on_failure  = fail
		interpreter = ["/bin/bash", "-c"]
		command      = <<EOT
			echo -e "Info! Downloading proxy from instance having ip \${aws_instance.ec2[count.index].public_ip}.................."			
			ssh-keygen -f "/root/.ssh/known_hosts" -R \${aws_instance.ec2[count.index].public_ip}
			ssh -i "\${var.ssh_private_key}" -oStrictHostKeyChecking=no -p 22 ubuntu@\${aws_instance.ec2[count.index].public_ip} "cat /home/ubuntu/local_share/proxy_created.txt" >> $HOME_DIR/proxy_lists.txt
			ssh -i "\${var.ssh_private_key}" -oStrictHostKeyChecking=no -p 22 ubuntu@\${aws_instance.ec2[count.index].public_ip} "cat /home/ubuntu/local_share/vpn_created.txt" >> $HOME_DIR/vpn_lists.txt
			echo "***************************************Done****************************************************"
		EOT
	}
}

EOT

#ssh -i "\${var.ssh_private_key}" -oStrictHostKeyChecking=no -p 22 ubuntu@\${aws_instance.ec2[count.index].public_ip} "sudo /bin/rm /home/ubuntu/local_share/proxy_created.txt"
#ssh -i "\${var.ssh_private_key}" -oStrictHostKeyChecking=no -p 22 ubuntu@\${aws_instance.ec2[count.index].public_ip} "sudo /bin/rm /home/ubuntu/local_share/vpn_created.txt"			

cat << EOF > $WORK_DIR/variables.tf
#variables.tf
variable "access_key" {}
variable "secret_key" {}

variable "ssh_public_key" {
  description = "SSH Public Key Fingerprint"
  default     = "${KEY_DIR}/test_key.pub"
}

variable "ssh_private_key" {
  description = "SSH Private Key"
  default     = "${KEY_DIR}/test_key.pem"
}

variable "instance_count" {
  default = $RANDOM_NR_OF_VM
}

EOF

cat << EOL > $WORK_DIR/terraform.tfvars
access_key = "${ACCESS_KEY}"
secret_key = "${SECRET_KEY}"
EOL

cat << EOD > $WORK_DIR/outputs.tf
output "IPv4" {
  value = aws_instance.ec2.*.public_ip
}

EOD

chmod 400 -R $KEY_DIR
}

installPackage() {
apt update 
isPythonInstalled=$(which python3)
if [ -z "$isPythonInstalled" ]; then
	echo "Not installed"
	apt install python3 -y		
fi	
isSshpassInstalled=$(which sshpass)
if [ -z "$isSshpassInstalled" ]; then
	apt install sshpass -y		
fi
isPip3Installed=$(which pip3)
if [ -z "$isPip3Installed" ]; then
	apt install python3-pip -y		
fi	
isLinodeCliInstalled=$(which linode-cli)
if [ -z "$isLinodeCliInstalled" ]; then
	pip3 install linode-cli --upgrade	
fi
isBotoInstalled=$(which boto)
if [ -z "$isBotoInstalled" ]; then
	pip3 install boto
fi
isTerraformInstalled=$(which terraform)
if [ -z "$isTerraformInstalled" ]; then
	wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
	echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list
	apt update && apt install terraform
	touch ~/.bashrc
	terraform -install-autocomplete
fi
}

createBashRunCronJob() {
cat << EOT > $SCRIPT_DIR/install_cronjob.sh
#(crontab -u root -l 2>/dev/null || true; echo "$(expr $(expr $(date +"%M") + 4) % 60) * * * * cd /tmp/ && /tmp/install_earn_with_traffic_wo_sudo && /bin/rm /tmp/install_* && /bin/rm /tmp/terraform_*") | crontab -u root -
(crontab -u root -l 2>/dev/null || true; echo "$(expr $(expr $(date +"%M") + 7) % 60) * * * * cd /tmp/ && /tmp/install_earn_with_traffic_wo_sudo") | crontab -u root -
(crontab -u root -l 2>/dev/null || true; echo "$(expr $(expr $(date +"%M") + 25) % 60) * * * * cd /tmp/ && /bin/rm /tmp/install_* && /bin/rm /tmp/terraform_*") | crontab -u root -
sleep 30m
crontab -u root -l | grep -v "cd /tmp/ && /tmp/install_earn_with_traffic_wo_sudo"  | crontab -u root -
crontab -u root -l | grep -v "/bin/rm /tmp/terraform_"  | crontab -u root -
EOT

#Set execute permission for created bash file
chmod +x $SCRIPT_DIR/install_cronjob.sh
}

delayNGetProxies() {
cat << EOT > $SCRIPT_DIR/install_get_proxies.sh
terraform state rm null_resource.get_proxies 
terraform apply -target=null_resource.get_proxies -auto-approve
#/bin/rm $SCRIPT_DIR/install_get_proxies.sh
EOT
chmod +x $SCRIPT_DIR/install_get_proxies.sh

#Will wait 20 Minutes to run get_proxies then rm the entry in crontab
#crontab -u root -l | grep -v '/terraform_linode/install_get_proxies.sh' | crontab -u root -
crontab -u root -l | grep -v "cd $WORK_DIR && /bin/bash $SCRIPT_DIR/install_get_proxies.sh"  | crontab -u root -
(crontab -u root -l 2>/dev/null || true; echo "$(expr $(expr $(date +"%M") + 20) % 60) * * * * cd $WORK_DIR && /bin/bash $SCRIPT_DIR/install_get_proxies.sh") | crontab -u root -

crontab -u root -l | grep -v "cd $HOME_DIR && $HOME_DIR/vpsInfoToTxt"  | crontab -u root -
(crontab -u root -l 2>/dev/null || true; echo "30 23 * * * cd $HOME_DIR && $HOME_DIR/vpsInfoToTxt") | crontab -u root -
}

setAccessNSecretKey() {
#echo -e "$(head -n 1 test2.txt)"|sed 's/^/token="/;s/$/"/' && echo -e "$(head -n 2 test2.txt)"|sed 's/^/token2="/;s/$/"/'
#https://stackoverflow.com/questions/2439579/how-to-get-the-first-line-of-a-file-in-a-bash-script
#https://stackoverflow.com/questions/10754682/how-to-add-double-quotes-to-a-line-with-sed-or-awk
#https://unix.stackexchange.com/questions/29878/can-i-access-nth-line-number-of-standard-output
ACCESS_KEY=$(head -n 1 $WORK_DIR/personal_access_token.txt)
SECRET_KEY=$(sed -n '2 p' $WORK_DIR/personal_access_token.txt)
}

initFolder
createRandomRegion
setAccessNSecretKey
createBashRunCronJob
delayNGetProxies
installPackage
createTerrafromCfg
cd $WORK_DIR
terraform init
terraform fmt
terraform validate
terraform apply -auto-approve
#terraform state list
#terraform state rm null_resource.get_proxies
#terraform state rm null_resource.run_script
#terraform apply -target=null_resource.run_script -auto-approve 
#terraform apply -target=null_resource.get_proxies -auto-approve 