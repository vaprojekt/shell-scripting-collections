#!/bin/bash

#https://stackoverflow.com/questions/23291920/inserting-date-into-mysql-table-using-shell-script
ARCH=$(dpkg --print-architecture)
DB_PASSWORD='your database password'
SMB_PASS='your samba password'
DIST=$(grep -Po "(?<=^ID=).+" /etc/os-release | sed 's/"//g')
HOME_DIR='/home/ubuntu'
DB_NAME='account_info'
DB_USERNAME='dbadmin'
DB_TABLE='server_info'
TIME_ZONE=$(date +"%Z")

if [ "$DIST" = "debian" ]; then
    HOME_DIR='/home/'$DIST
fi

if [ ! -d "$HOME_DIR" ]; then
   mkdir $HOME_DIR
fi

VPS_INFO=$HOME_DIR/local_share/vps_info.txt
BAD_PROXYS=$HOME_DIR/local_share/bad.txt


configMariaDB() {
isMySQLInstalled=$(which mysql_secure_installation)
if [ -z "$isMySQLInstalled" ]; then
	apt-get install mariadb-server -y
	EXPECT_DIR=$(which expect)
	MYSQL_DIR=$(which mysql_secure_installation)
	$EXPECT_DIR << EOF
set timeout -1
spawn $MYSQL_DIR
expect "Enter current password for root"
send "\r"
expect "root password"
send "Y\r"
expect "New password"
send "$DB_PASSWORD\r"
expect "Re-enter new password"
send "$DB_PASSWORD\r"
expect "Remove anonymous users"
send "Y\r"
expect "Disallow root"
send "Y\r"
expect "Remove test database"
send "Y\r"
expect "Reload privilege"
send "Y\r"
expect eof
exit
EOF
fi
}

installPackage() {
apt update 
isCurlInstalled=$(which curl)
if [ -z "$isCurlInstalled" ]; then
	echo "Not installed"
	apt-get install curl -y		
fi	
isExpectInstalled=$(which expect)
if [ -z "$isExpectInstalled" ]; then
	apt-get install expect -y		
fi
apt-get install cifs-utils -y
configMariaDB
}

resetSambaConfig() {
/bin/umount -f $HOME_DIR/local_share
/bin/rmdir $HOME_DIR/local_share
}

configSambaToLoadProxy() {
mkdir $HOME_DIR/local_share
EXPECT_DIR=$(which expect)
$EXPECT_DIR << EOF
set timeout -1
spawn mount -t cifs -o username=proxyacc //samba_server_ip/share $HOME_DIR/local_share
expect "Password"
send "${SMB_PASS}\r"
expect eof
exit
EOF
}

createDBTable() {
mysql $DB_NAME -ss -N -e "
CREATE TABLE \`${DB_TABLE}\` (
\`id\` int(2) NOT NULL AUTO_INCREMENT,
\`provider\` varchar(30) DEFAULT NULL,
\`provider_id\` int(2) DEFAULT NULL,
\`ip\` varchar(15) DEFAULT NULL,
\`username\` varchar(10) DEFAULT NULL,
\`password\` varchar(40) DEFAULT NULL,
\`country\` varchar(40) DEFAULT NULL,
\`city\` varchar(30) DEFAULT NULL,
\`postal\` varchar(15) DEFAULT NULL,
\`time_created\` datetime NOT NULL,
PRIMARY KEY (\`id\`)
);
"
}

#https://stackoverflow.com/questions/32869275/a-better-way-to-execute-multiple-mysql-commands-using-shell-script
#https://stackoverflow.com/questions/47712111/how-to-check-if-a-table-exists-in-a-mysql-database-using-shell-script
#https://stackoverflow.com/questions/62993611/append-string-and-special-character-in-the-output-of-jq-command
initDataBase() {
#Check if account database existed

IS_DB_EXISTED=`mysqlshow | grep -v Database | grep -o $DB_NAME`
if [ -z "$IS_DB_EXISTED" ]; then
	mysql << EOF
CREATE DATABASE ${DB_NAME};
GRANT ALL ON ${DB_NAME}.* to '${DB_USERNAME}'@'localhost' IDENTIFIED BY '${DB_PASSWORD}';
FLUSH PRIVILEGES;
EOF
fi

SQL_EXISTS=$(printf 'SHOW TABLES LIKE "%s"' "$DB_TABLE")

# Check if table exists
if [[ $(mysql -e "$SQL_EXISTS" $DB_NAME) ]]
then
    echo "Table exists ..."
else
    echo "Table not exists ..."
	createDBTable
fi
}

initVariable4DB() {
initDataBase
_null=null
SQL_QUERY="SELECT id FROM \`${DB_TABLE}\` WHERE id = (SELECT MAX(id) FROM \`${DB_TABLE}\`)"
FIELDVALUE=$(mysql $DB_NAME -ss -N -e "${SQL_QUERY}")
}

#Tren server deploy aws hoac lin/do luu du lieu terraform ra file text cp len samba server 
#terraform output -json |jq -r --arg prefix "okok" '.IPv4.value[]  + $prefix +  .Password.value + "okok${PROVIDER}" + "okok${USERNAME}" + "okok${PROVIDER_ID}"' >> /home/ubuntu/local_share/vps_info.txt
#| awk -F 'okok' '{print $2}'
#grep -Eo '[0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}' proxies 
#https://realsunjester.wordpress.com/2019/02/18/using-nmap-to-scan-for-proxy-servers/

#Add proxy_data from good.txt to database
addDataToDB() {
# CHECK FILE WITH VPS_INFO IF EXIST #
if ! [ -f $VPS_INFO > /dev/null ]
then
  echo -e $RED"File with vps_info not found ($VPS_INFO)"$DEF
  exit 1
fi
# END OF CHECK FILE WITH VPS_INFO IF EXIST #

# GET VPS_INFO #
for VPS in $(<$VPS_INFO)
do  
  if [[ "${VPS:0:1}" != "#" ]]
  then
    unset USER PASS
    IP=$(echo $VPS | awk -F 'okok' '{print $1}')
    PASS=$(echo $VPS | awk -F 'okok' '{print $2}')
    PROVIDER=$(echo $VPS | awk -F 'okok' '{print $3}')
    USER=$(echo $VPS | awk -F 'okok' '{print $4}')
    PROVIDER_ID=$(echo $VPS | awk -F 'okok' '{print $5}')
	COUNTRY=$(echo $VPS | awk -F 'okok' '{print $6}')
	CITY=$(echo $VPS | awk -F 'okok' '{print $7}')
	ZIPCODE=$(echo $VPS | awk -F 'okok' '{print $8}')
	TIME_CREATED=$(TZ=$TIME_ZONE date +'%F %T')
	
	#https://askubuntu.com/questions/537956/sed-to-delete-blank-spaces
	#sed s/'\s'//g filename
	#Add info about country and region an city for each server
	#COUNTRY=$(curl -s ipinfo.io/$IP | jq ".country")
	#CITY=$(curl -s ipinfo.io/$IP | jq ".city")
	#REGION=$(curl -s ipinfo.io/$IP | jq ".region")
	#ZIPCODE=$(curl -s ipinfo.io/$IP | jq ".postal")
	#LOC=$(curl -s ipinfo.io/$IP | jq ".loc")
	#TiZo=$(curl -s ipinfo.io/$IP | jq ".timezone")
	
	mysql $DB_NAME << EOF
INSERT INTO \`${DB_TABLE}\` (provider, provider_id, ip, username, password, country, city, postal, time_created) VALUES ('$PROVIDER', $PROVIDER_ID, '$IP', '$USER', '$PASS', '$COUNTRY', '$CITY', '$ZIPCODE', '$TIME_CREATED');
EOF
  fi
done
# END OF GET VPS_INFO #
}

rmDataFromDB() {
# CHECK FILE WITH VPS_INFO IF EXIST #
if ! [ -f $BAD_PROXYS > /dev/null ]
then
  echo -e $RED"File with proxy not found ($BAD_PROXYS)"$DEF
  exit 1
fi
# END OF CHECK FILE WITH VPS_INFO IF EXIST #

echo "Remove any whitespace inside text file"
sed -i s/'\s'//g $VPS_INFO

# CHECK PROXY #
for PROXY in $(<$BAD_PROXYS)
do  
  if [[ "${PROXY:0:1}" != "#" ]]
  then
    IP=$(echo $PROXY | awk -F: '{print $1}')

	mysql $DB_NAME << EOF
DELETE FROM \`${DB_TABLE}\` WHERE ip='$IP';
EOF
  fi
done
# END OF CHECK PROXY #
}

rmDuplicatesDataFromDB() {
mysql $DB_NAME << EOF
DELETE c1 FROM \`${DB_TABLE}\` c1, \`${DB_TABLE}\` c2 
WHERE
    c1.id > c2.id AND 
    c1.ip = c2.ip;
EOF
echo "Update id after delete duplicates records"
mysql $DB_NAME -ss -N -e "
SET  @num := 0;
UPDATE \`${DB_TABLE}\` SET id = @num := (@num+1);
ALTER TABLE \`${DB_TABLE}\` AUTO_INCREMENT = 1;
"
}

#Permission on Samba not work ok if delete file, it still exists
#Only root on samba server can delete file
clearInfo() {
/bin/rm $VPS_INFO
}

installPackage
resetSambaConfig
configSambaToLoadProxy
initVariable4DB
addDataToDB
rmDuplicatesDataFromDB
#rmDataFromDB
#clearInfo

#Cronjob On DB Server
#30 2 * * * cd /home/debian && /home/debian/vpsInfoToDB
#chown proxyacc:proxyacc /home/proxyacc/saved_proxy/*.txt
#chmod 700 /home/proxyacc/saved_proxy/*.txt